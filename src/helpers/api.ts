import type { EvaluationRun, GroundTruth, Workflow } from "@/types"

const baseUrl = 'http://localhost:8084/api'

async function getWorkflows(): Promise<Workflow[]> {
    return await request(baseUrl + '/workflows')
}

async function getGroundTruth(): Promise<GroundTruth[]> {
    return await request(baseUrl + '/gt')
}

async function getRuns(gtId?: string, workflowId?: string): Promise<EvaluationRun[]> {
    let path = `${baseUrl}/runs`

    if (gtId) path += `/${gtId}`
    if (workflowId) path += `/${workflowId}`

    return await request(path)
}

async function getLatestRuns(gtId?: string, workflowId?: string): Promise<EvaluationRun[]> {
    let path = `${baseUrl}/runs`

    if (gtId) path += `/${gtId}`
    if (workflowId) path += `/${workflowId}`

    path += '/latest'

    return await request(path)
}

async function request (url: string) {
    const response = await fetch(url)
    return await response.json()
}

export default {
    getWorkflows,
    getGroundTruth,
    getRuns,
    getLatestRuns
}
